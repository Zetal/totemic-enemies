package com.zetal.totemicenemies.tileentity;

import com.zetal.totemicenemies.TotemicEnemiesBlocks;

import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.MobSpawnerBaseLogic;
import net.minecraft.tileentity.TileEntityMobSpawner;
import net.minecraft.util.ITickable;
import net.minecraft.util.WeightedSpawnerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityTotem extends TileEntityMobSpawner implements ITickable
{
    private final TotemSpawnerLogic spawnerLogic = new TotemSpawnerLogic()
    {
        @Override
        public void broadcastEvent(int id)
        {
        	TileEntityTotem.this.world.addBlockEvent(TileEntityTotem.this.pos, TotemicEnemiesBlocks.TOTEM, id, 0);
        }
        @Override
        public World getSpawnerWorld()
        {
            return TileEntityTotem.this.world;
        }
        @Override
        public BlockPos getSpawnerPosition()
        {
            return TileEntityTotem.this.pos;
        }
        @Override
        public void setNextSpawnData(WeightedSpawnerEntity p_184993_1_)
        {
            super.setNextSpawnData(p_184993_1_);

            if (this.getSpawnerWorld() != null)
            {
                IBlockState iblockstate = this.getSpawnerWorld().getBlockState(this.getSpawnerPosition());
                this.getSpawnerWorld().notifyBlockUpdate(TileEntityTotem.this.pos, iblockstate, iblockstate, 4);
            }
        }
    };

	@Override
	public void readFromNBT(NBTTagCompound compound)
	{
		super.readFromNBT(compound);
		this.spawnerLogic.readFromNBT(compound);
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		super.writeToNBT(compound);
		this.spawnerLogic.writeToNBT(compound);
		return compound;
	}

	@Override
	public void update()
	{
		this.spawnerLogic.updateSpawner();
	}

    @SideOnly(Side.CLIENT)
    public double getMaxRenderDistanceSquared()
    {
        return 65536.0D;
    }

	@Override
	public boolean receiveClientEvent(int id, int type)
	{
		return this.spawnerLogic.setDelayToMin(id) ? true : super.receiveClientEvent(id, type);
	}

	@Override
	public TotemSpawnerLogic getSpawnerBaseLogic()
	{
		return this.spawnerLogic;
	}

	@Override
	public SPacketUpdateTileEntity getUpdatePacket()
	{
		return new SPacketUpdateTileEntity(pos, 0, this.getUpdateTag());
	}

	@Override
	public NBTTagCompound getUpdateTag()
	{
		return this.writeToNBT(new NBTTagCompound());
	}

	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
	{
		NBTTagCompound nbttagcompound = pkt.getNbtCompound();
		this.readFromNBT(nbttagcompound);
	}
}
