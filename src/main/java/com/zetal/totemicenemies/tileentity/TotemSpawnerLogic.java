package com.zetal.totemicenemies.tileentity;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;
import com.zetal.totemicenemies.TotemicEnemiesMod;
import com.zetal.totemicenemies.config.TotemicConfig;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.monster.IMob;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.MobSpawnerBaseLogic;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.WeightedRandom;
import net.minecraft.util.WeightedSpawnerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.storage.AnvilChunkLoader;
import net.minecraft.world.gen.ChunkProviderServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class TotemSpawnerLogic extends MobSpawnerBaseLogic
{
	/** The delay to spawn. */
	private int spawnDelay = 20;
	/** List of potential entities to spawn */
	private final List<WeightedSpawnerEntity> potentialSpawns = Lists.<WeightedSpawnerEntity> newArrayList();
	private WeightedSpawnerEntity spawnData = null;

	public boolean hasPotentialSpawns()
	{
		return !potentialSpawns.isEmpty();
	}

	public void setPotentialSpawns(List<WeightedSpawnerEntity> newList)
	{
		potentialSpawns.clear();
		potentialSpawns.addAll(newList);
	}

	public void addPotentialSpawns(List<WeightedSpawnerEntity> newList)
	{
		potentialSpawns.addAll(newList);
	}

	public List<WeightedSpawnerEntity> getPotentialSpawns()
	{
		return potentialSpawns;
	}

	public void addPotentialSpawn(WeightedSpawnerEntity wse)
	{
		if (wse != null)
		{
			boolean matched = false;
			for (WeightedSpawnerEntity w : potentialSpawns)
			{
				if (w.getNbt().getString("id").equals(wse.getNbt().getString("id")))
				{
					matched = true;
					break;
				}
			}

			if (!matched)
			{
				potentialSpawns.add(wse);
			}
		}
	}

	public void removePotentialSpawn(WeightedSpawnerEntity wse)
	{
		if (wse != null)
		{
			potentialSpawns.remove(wse);
		}
	}

	public void removePotentialSpawn(String id)
	{
		if (id != null)
		{
			WeightedSpawnerEntity wse = null;
			for (WeightedSpawnerEntity w : potentialSpawns)
			{
				if (w.getNbt().getString("id").equals(id))
				{
					wse = w;
					break;
				}
			}
			potentialSpawns.remove(wse);
		}
	}

	/**
	 * Returns true if there's a player close enough to this mob spawner to activate it.
	 */
	private boolean isActivated()
	{
		BlockPos blockpos = this.getSpawnerPosition();
		return this.getSpawnerWorld().isAnyPlayerWithinRangeAt((double) blockpos.getX() + 0.5D, (double) blockpos.getY() + 0.5D, (double) blockpos.getZ() + 0.5D, (double) TotemicConfig.activatingRangeFromPlayer);
	}

	@Override
	public void updateSpawner()
	{
		if (this.isActivated())
		{
			BlockPos blockpos = this.getSpawnerPosition();

			if (this.getSpawnerWorld().isRemote)
			{
				if (this.hasPotentialSpawns())
				{
					double d3 = (double) ((float) blockpos.getX() + this.getSpawnerWorld().rand.nextFloat());
					double d4 = (double) ((float) blockpos.getY() + this.getSpawnerWorld().rand.nextFloat());
					double d5 = (double) ((float) blockpos.getZ() + this.getSpawnerWorld().rand.nextFloat());
					this.getSpawnerWorld().spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d3, d4, d5, 0.0D, 0.0D, 0.0D);
					this.getSpawnerWorld().spawnParticle(EnumParticleTypes.FLAME, d3, d4, d5, 0.0D, 0.0D, 0.0D);

					if (this.spawnDelay > 0)
					{
						--this.spawnDelay;
					}
				}
			}
			else
			{
				if (this.spawnDelay == -1)
				{
					this.resetTimer();
				}

				if (this.spawnDelay > 0)
				{
					--this.spawnDelay;
					return;
				}

				boolean flag = false;

				for (int i = 0; i < TotemicConfig.spawnCount; ++i)
				{
					this.setNextSpawnUsingPotential();

					if (this.hasPotentialSpawns())
					{
						NBTTagCompound nbttagcompound = this.spawnData.getNbt();
						World world = this.getSpawnerWorld();
						double d0 = (double) blockpos.getX() + (world.rand.nextDouble() - world.rand.nextDouble()) * (double) TotemicConfig.spawnRange + 0.5D;
						double d1 = (double) (blockpos.getY() + world.rand.nextInt(3) - 1);
						double d2 = (double) blockpos.getZ() + (world.rand.nextDouble() - world.rand.nextDouble()) * (double) TotemicConfig.spawnRange + 0.5D;
						Entity entity = AnvilChunkLoader.readWorldEntityPos(nbttagcompound, world, d0, d1, d2, false);

						if (entity == null)
						{
							return;
						}

						AxisAlignedBB aabb = (new AxisAlignedBB((double) blockpos.getX(), (double) blockpos.getY(), (double) blockpos.getZ(), (double) (blockpos.getX() + 1), (double) (blockpos.getY() + 1), (double) (blockpos.getZ() + 1)));
						List<EntityLiving> entityList = world.getEntitiesWithinAABB(EntityLiving.class, aabb.grow((double) TotemicConfig.entityCheckRange));
						List<EntityLiving> removes = new ArrayList<EntityLiving>();
						for (int j = 0; j < entityList.size(); j++)
						{
							if (!(entityList.get(j) instanceof IMob))
							{
								removes.add(entityList.get(j));
							}
						}
						entityList.removeAll(removes);
						int k = entityList.size();

						if (k >= TotemicConfig.maxNearbyEntities)
						{
							this.resetTimer();
							return;
						}

						EntityLiving entityliving = entity instanceof EntityLiving ? (EntityLiving) entity : null;
						entityliving.setLocationAndAngles(entity.posX, entity.posY, entity.posZ, world.rand.nextFloat() * 360.0F, 0.0F);
						entityliving.onInitialSpawn(world.getDifficultyForLocation(new BlockPos(entityliving)), null);

						if (entityliving == null || net.minecraftforge.event.ForgeEventFactory.canEntitySpawnSpawner(entityliving, getSpawnerWorld(), (float) entity.posX, (float) entity.posY, (float) entity.posZ))
						{
							if (this.spawnData.getNbt().getSize() == 1 && this.spawnData.getNbt().hasKey("id", 8) && entity instanceof EntityLiving)
							{
								if (!net.minecraftforge.event.ForgeEventFactory.doSpecialSpawn(entityliving, this.getSpawnerWorld(), (float) entity.posX, (float) entity.posY, (float) entity.posZ)) ((EntityLiving) entity).onInitialSpawn(world.getDifficultyForLocation(new BlockPos(entity)), (IEntityLivingData) null);
							}

							AnvilChunkLoader.spawnEntity(entity, world);
							world.playEvent(2004, blockpos, 0);

							if (entityliving != null)
							{
								entityliving.spawnExplosionParticle();
							}

							flag = true;
						}
					}
				}

				if (flag)
				{
					this.resetTimer();
				}
			}
		}
	}

	private void setNextSpawnUsingPotential()
	{
		if (hasPotentialSpawns())
		{
			this.setNextSpawnData((WeightedSpawnerEntity) WeightedRandom.getRandomItem(this.getSpawnerWorld().rand, this.potentialSpawns));
		}
		else
		{
			if (this.getSpawnerWorld() instanceof WorldServer)
			{
				ChunkProviderServer prov = ((WorldServer) this.getSpawnerWorld()).getChunkProvider();
				this.setPotentialSpawns(TotemicEnemiesMod.getWSEListFromBiomeSLEList(prov.getPossibleCreatures(EnumCreatureType.MONSTER, this.getSpawnerPosition())));
			}
			else
			{
				System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
			}

			if (hasPotentialSpawns())
			{
				this.setNextSpawnData((WeightedSpawnerEntity) WeightedRandom.getRandomItem(this.getSpawnerWorld().rand, this.potentialSpawns));
			}
		}
	}

	private void resetTimer()
	{
		if (TotemicConfig.maxSpawnDelay <= TotemicConfig.minSpawnDelay)
		{
			this.spawnDelay = TotemicConfig.minSpawnDelay;
		}
		else
		{
			int i = TotemicConfig.maxSpawnDelay - TotemicConfig.minSpawnDelay;
			this.spawnDelay = TotemicConfig.minSpawnDelay + this.getSpawnerWorld().rand.nextInt(i);
		}

		setNextSpawnUsingPotential();

		this.broadcastEvent(1);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.spawnDelay = nbt.getShort("Delay");
		this.potentialSpawns.clear();

		if (nbt.hasKey("SpawnPotentials", 9))
		{
			NBTTagList nbttaglist = nbt.getTagList("SpawnPotentials", 10);

			for (int i = 0; i < nbttaglist.tagCount(); ++i)
			{
				this.potentialSpawns.add(new WeightedSpawnerEntity(nbttaglist.getCompoundTagAt(i)));
			}
		}
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound p_189530_1_)
	{
		p_189530_1_.setShort("Delay", (short) this.spawnDelay);
		NBTTagList nbttaglist = new NBTTagList();

		if (hasPotentialSpawns())
		{
			for (WeightedSpawnerEntity weightedspawnerentity : this.potentialSpawns)
			{
				nbttaglist.appendTag(weightedspawnerentity.toCompoundTag());
			}
		}

		p_189530_1_.setTag("SpawnPotentials", nbttaglist);
		return p_189530_1_;
	}

	/**
	 * Sets the delay to minDelay if parameter given is 1, else return false.
	 */
	@Override
	public boolean setDelayToMin(int delay)
	{
		if (delay == 1 && this.getSpawnerWorld().isRemote)
		{
			this.spawnDelay = TotemicConfig.minSpawnDelay;
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public void setNextSpawnData(WeightedSpawnerEntity p_184993_1_)
	{
		this.spawnData = p_184993_1_;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Entity getCachedEntity()
	{
		return null;
	}
}
