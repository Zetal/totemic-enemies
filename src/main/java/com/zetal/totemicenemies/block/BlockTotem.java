package com.zetal.totemicenemies.block;

import javax.annotation.Nullable;

import com.zetal.totemicenemies.TotemicEnemiesBlocks;
import com.zetal.totemicenemies.tileentity.TileEntityTotem;

import net.minecraft.block.BlockMobSpawner;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockTotem extends BlockMobSpawner
{
	public static final PropertyDirection FACING = PropertyDirection.create("facing", EnumFacing.Plane.HORIZONTAL);

	protected static final AxisAlignedBB NORTH_AABB = new AxisAlignedBB(0.1875D, 0D, 0D, 0.8125D, 1.2D, 1D);
	protected static final AxisAlignedBB SOUTH_AABB = new AxisAlignedBB(0.1875D, 0D, 0D, 0.8125D, 1.2D, 1D);
	protected static final AxisAlignedBB WEST_AABB = new AxisAlignedBB(0D, 0D, 0.1875D, 1D, 1.2D, 0.8125D);
	protected static final AxisAlignedBB EAST_AABB = new AxisAlignedBB(0D, 0D, 0.1875D, 1D, 1.2D, 0.8125D);
	protected static final AxisAlignedBB DEFAULT_AABB = new AxisAlignedBB(0.1875D, 0D, 0D, 0.8125D, 1.2D, 1D);
	
	public BlockTotem()
	{
		super();
		this.setSoundType(SoundType.WOOD);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH));
		this.setHardness(6.0F);
		this.setHarvestLevel("axe", 0, this.getDefaultState());
	}

	@Override
    @SideOnly(Side.CLIENT)
    public boolean hasCustomBreakingProgress(IBlockState state)
    {
        return true;
    }

	@Override
    public boolean isToolEffective(String type, IBlockState state)
    {
        return type != null && type.equals(getHarvestTool(getDefaultState()));
    }

	@Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
    {
    	return new ItemStack(Item.getItemFromBlock(this));
    }

	@Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return getCollisionBoundingBox(state, source, pos);
    }

	@Override
	public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
	{
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();
		EnumFacing face = (EnumFacing) blockState.getValue(FACING);
		if (face == EnumFacing.NORTH)
		{
			return this.NORTH_AABB;
		}
		else if (face == EnumFacing.SOUTH)
		{
			return this.SOUTH_AABB;
		}
		else if (face == EnumFacing.WEST)
		{
			return this.WEST_AABB;
		}
		else if (face == EnumFacing.EAST)
		{
			return this.EAST_AABB;
		}
		return this.DEFAULT_AABB;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public Material getMaterial(IBlockState state)
	{
		return Material.PISTON;
	}

	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileEntityTotem();
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		EnumFacing enumfacing = EnumFacing.getFront(meta);

		if (enumfacing.getAxis() == EnumFacing.Axis.Y)
		{
			enumfacing = EnumFacing.NORTH;
		}

		return this.getDefaultState().withProperty(FACING, enumfacing);
	}

	/**
	 * Convert the BlockState into the correct metadata value
	 */
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return ((EnumFacing) state.getValue(FACING)).getIndex();
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { FACING });
	}
}
