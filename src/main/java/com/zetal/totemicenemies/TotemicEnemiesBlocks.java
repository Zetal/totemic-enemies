package com.zetal.totemicenemies;

import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.zetal.totemicenemies.block.BlockTotem;
import com.zetal.totemicenemies.tileentity.TileEntityTotem;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;
import net.minecraftforge.registries.IForgeRegistry;

@ObjectHolder(TotemicEnemiesMod.MODID)
public class TotemicEnemiesBlocks
{
	public static final BlockTotem TOTEM = (BlockTotem) new BlockTotem().setRegistryName(TotemicEnemiesMod.MODID, "totem").setUnlocalizedName("totem");

	@Mod.EventBusSubscriber(modid = TotemicEnemiesMod.MODID)
	public static class RegistrationHandler
	{
		public static final Set<ItemBlock> ITEM_BLOCKS = new HashSet<>();

		@SubscribeEvent
		public static void registerBlocks(final RegistryEvent.Register<Block> event)
		{
			final IForgeRegistry<Block> registry = event.getRegistry();
			registry.register(TOTEM);
		}

		@SubscribeEvent
		public static void registerItemBlocks(final RegistryEvent.Register<Item> event)
		{
			final IForgeRegistry<Item> registry = event.getRegistry();

			ItemBlock totemItem = new ItemBlock(TOTEM);
			final ResourceLocation registryName = Preconditions.checkNotNull(TOTEM.getRegistryName(), "Block %s has null registry name", TOTEM);
			registry.register(totemItem.setRegistryName(registryName));
			ITEM_BLOCKS.add(totemItem);

			registerTileEntities();
		}
	}

	private static void registerTileEntities()
	{
		registerTileEntity(TileEntityTotem.class, "TileEntityTotem");
	}

	private static void registerTileEntity(final Class<? extends TileEntity> tileEntityClass, final String name)
	{
		GameRegistry.registerTileEntity(tileEntityClass, TotemicEnemiesMod.MODID + ":" + name);
	}
}
