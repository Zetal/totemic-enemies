package com.zetal.totemicenemies;

import java.util.ArrayList;
import java.util.List;

import com.zetal.totemicenemies.handler.TotemicEventHandler;

import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.WeightedSpawnerEntity;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.SpawnListEntry;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = TotemicEnemiesMod.MODID, version = TotemicEnemiesMod.VERSION)
public class TotemicEnemiesMod
{
    @SidedProxy(serverSide= "com.zetal.totemicenemies.TotemicEnemiesCommonProxy", clientSide = "com.zetal.totemicenemies.TotemicEnemiesClientProxy")
    public static TotemicEnemiesCommonProxy proxy;
    
    public static final String MODID = "totemicenemies";
    public static final String VERSION = "1.0.8";
    
	@Instance(MODID)
	public static TotemicEnemiesMod instance;

	@EventHandler
    public void init(FMLInitializationEvent event)
    {
		MinecraftForge.EVENT_BUS.register(new TotemicEventHandler());
		MinecraftForge.TERRAIN_GEN_BUS.register(new TotemicEventHandler());
    }

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		proxy.registerRenderInformation();
	}
	
	public static List<WeightedSpawnerEntity> getWSEListFromBiomeSLEList(List<Biome.SpawnListEntry> biomeSLEList)
	{
		List<WeightedSpawnerEntity> potential = new ArrayList<WeightedSpawnerEntity>();

		for (SpawnListEntry e : biomeSLEList)
		{
			try
			{
		        NBTTagCompound nbt = new NBTTagCompound();
		        nbt.setString("id", EntityList.getKey(e.entityClass).toString());
				WeightedSpawnerEntity wse = new WeightedSpawnerEntity(e.itemWeight, nbt);
				potential.add(wse);
			}
			catch (Exception e1)
			{
				e1.printStackTrace();
			}
		}
		
		return potential;
	}
}
