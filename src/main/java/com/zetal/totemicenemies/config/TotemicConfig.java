package com.zetal.totemicenemies.config;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import com.zetal.totemicenemies.TotemicEnemiesBlocks;
import com.zetal.totemicenemies.TotemicEnemiesMod;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = TotemicEnemiesMod.MODID)
@Config.LangKey("totemic.config.title")
public class TotemicConfig
{
	@Config.Name("Totem Spawn Chance")
	@Config.Comment("Chance for a totem to spawn in any given chunk.")
	public static double spawnerChance = 0.3334;

	@Config.Name("Totem Height Spawn Scan")
	@Config.Comment("How far to adjust the height each time when spawning Totems. Higher value means less underground Totems.")
	@Config.RangeInt(min = 0)
	public static int verticalScanDiff = 12;

	@Config.Name("Totem Safe Start Size")
	@Config.Comment("The size of the starting area to avoid spawning totems, in squared chunks.")
	@Config.RequiresWorldRestart
	public static int safeSpawnArea = 6;

	@Config.Name("Totem Toughness")
	@Config.Comment("How hard it is to break a Totem.")
	public static int totemHardness = 6;

	@Config.Name("Totem Spawn Count")
	@Config.Comment("Number of enemies spawned per totem spawn cycle.")
	public static int spawnCount = 1;

	@Config.Name("Totem Spawn Spread")
	@Config.Comment("How far from a Totem an enemy can be spawned.")
	public static int spawnRange = 12;

	@Config.Name("Totem Activation Range")
	@Config.Comment("Range in blocks at which Totem starts spawning enemies.")
	public static int activatingRangeFromPlayer = 48;

	@Config.Name("Max Enemies Near Totem")
	@Config.Comment("Maximum number of nearby enemies.")
	public static int maxNearbyEntities = 6;

	@Config.Name("Max Enemies Near Totem Range")
	@Config.Comment("How far to check near a totem for max enemies.")
	public static int entityCheckRange = 16;

	@Config.Name("Min Totem Spawn Delay")
	@Config.Comment("Minimum delay on a Totems enemy spawn cycle.")
	public static int minSpawnDelay = 100;

	@Config.Name("Max Totem Spawn Delay")
	@Config.Comment("Maximum delay on a Totems enemy spawn cycle.")
	public static int maxSpawnDelay = 400;

	@Config.Name("Dimension Blacklist")
	@Config.Comment("Comma-separated list of dimension ID's that this mod should NOT effect." + "\n" + "Example:1,-1" + "\n" + "Warning: Deviations from the required format can cause errors.")
	public static String dimensions = "";

	@Mod.EventBusSubscriber
	private static class EventHandler
	{
		/**
		 * Inject the new values and save to the config file when the config has been changed from the GUI.
		 *
		 * @param event
		 *            The event
		 */
		@SubscribeEvent
		public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event)
		{
			if (event.getModID().equals(TotemicEnemiesMod.MODID))
			{
				ConfigManager.sync(TotemicEnemiesMod.MODID, Config.Type.INSTANCE);
				TotemicEnemiesBlocks.TOTEM.setHardness(totemHardness);
			}
		}
	}

	public static int[] getDimensionBlacklist()
	{
		String dims = dimensions.replace(" ", "");
		String[] d = StringUtils.split(dims, ",");
		int[] results = new int[d.length];
		for (int i = 0; i < d.length; i++)
		{
			try
			{
				results[i] = Integer.parseInt(d[i]);
			}
			catch (NumberFormatException nfe)
			{
				System.err.println(nfe.getMessage());
			}
		}
		return results;
	}
}
