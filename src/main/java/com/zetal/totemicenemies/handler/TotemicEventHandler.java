package com.zetal.totemicenemies.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.zetal.totemicenemies.TotemicEnemiesBlocks;
import com.zetal.totemicenemies.block.BlockTotem;
import com.zetal.totemicenemies.config.TotemicConfig;
import com.zetal.totemicenemies.tileentity.TileEntityTotem;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.monster.IMob;
import net.minecraft.init.Blocks;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.WeightedSpawnerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldEntitySpawner;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.event.terraingen.PopulateChunkEvent;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class TotemicEventHandler
{
	private Random rand;

	@SubscribeEvent
	public void onClassicLivingSpawn(LivingSpawnEvent.CheckSpawn event)
	{
		if (!isDimensionBlacklisted(event.getWorld().provider.getDimension()))
		{
			if (event.getEntityLiving() instanceof IMob)
			{
				EntityLiving living = (EntityLiving) event.getEntityLiving();
				if (!event.isSpawner())
				{
					event.setResult(Result.DENY);
				}
				else
				{
					if (living.isNotColliding())
					{
						event.setResult(Result.ALLOW);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onLivingDespawn(LivingSpawnEvent.AllowDespawn event)
	{
		if (!isDimensionBlacklisted(event.getWorld().provider.getDimension()))
		{
			if (event.getEntityLiving() instanceof IMob)
			{
				event.setResult(Result.DENY);
			}
		}
	}

	@SubscribeEvent
	public void onLivingUpdate(LivingUpdateEvent event)
	{
		if (!isDimensionBlacklisted(event.getEntityLiving().getEntityWorld().provider.getDimension()))
		{
			if (event.getEntityLiving() instanceof IMob)
			{
				EntityLiving mob = (EntityLiving) event.getEntityLiving();
				mob.extinguish();
				mob.enablePersistence();
				if (mob.getNavigator() instanceof PathNavigateGround)
				{
					((PathNavigateGround) mob.getNavigator()).setAvoidSun(false);
				}
			}
		}
	}

	@SubscribeEvent
	public void onChunkGenerate(PopulateChunkEvent.Pre event)
	{
		if (!isDimensionBlacklisted(event.getWorld().provider.getDimension()))
		{
			World world = event.getWorld();
			Chunk chunk = world.getChunkFromChunkCoords(event.getChunkX(), event.getChunkZ());
			int i = event.getChunkX() * 16;
			int j = event.getChunkZ() * 16;
			BlockPos blockpos = new BlockPos(i, chunk.getTopFilledSegment(), j);
			Biome biome = world.getBiome(blockpos.add(16, 0, 16));
			if (rand == null)
			{
				rand = new Random(world.getSeed());
			}

			AxisAlignedBB bb = new AxisAlignedBB(world.getSpawnPoint().add(-TotemicConfig.safeSpawnArea * 8, 0, -TotemicConfig.safeSpawnArea * 8), world.getSpawnPoint().add(TotemicConfig.safeSpawnArea * 8, 0, TotemicConfig.safeSpawnArea * 8));

			if (bb.intersects(blockpos.getX(), 0, blockpos.getZ(), blockpos.getX() + 16, 256, blockpos.getZ() + 16))
			{
				return;
			}

			BlockPos posSurface = blockpos.add(0, 2, 0);
			while (posSurface.getY() > 16)
			{
				if (this.rand.nextFloat() < TotemicConfig.spawnerChance)
				{
					performTotemSpawning(world, biome, posSurface, rand);
				}
				posSurface = posSurface.add(0, -TotemicConfig.verticalScanDiff, 0);
			}
		}
	}

	public static boolean isDimensionBlacklisted(int dimensionId)
	{
		int[] blacklist = TotemicConfig.getDimensionBlacklist();
		for (int i = 0; i < blacklist.length; i++)
		{
			if (blacklist[i] == dimensionId)
			{
				return true;
			}
		}
		return false;
	}

	public static void performTotemSpawning(World world, Biome biome, BlockPos blockpos, Random rand)
	{
		for (int i = 0; i < 8; i++)
		{
			BlockPos randpos = blockpos.add(1 + rand.nextInt(15), 0, 1 + rand.nextInt(15));
			randpos = getFirstSolidBeneath(world, randpos);
			if (canCreatureTypeSpawnAtLocation(EntityLiving.SpawnPlacementType.ON_GROUND, world, randpos))
			{
				world.setBlockState(randpos, TotemicEnemiesBlocks.TOTEM.getDefaultState().withProperty(BlockTotem.FACING, EnumFacing.getHorizontal(rand.nextInt(EnumFacing.HORIZONTALS.length))), 3);
				TileEntity tileentity = world.getTileEntity(randpos);

				if (tileentity instanceof TileEntityTotem)
				{
					TileEntityTotem tot = (TileEntityTotem) tileentity;
				}
				break;
			}
		}
	}

	public static BlockPos getFirstSolidBeneath(World world, BlockPos pos)
	{
		Chunk chunk = world.getChunkFromBlockCoords(pos);
		BlockPos upperPos;
		BlockPos lowerPos;

		for (upperPos = new BlockPos(pos.getX(), pos.getY(), pos.getZ()); pos.getY() - upperPos.getY() <= TotemicConfig.verticalScanDiff; upperPos = lowerPos)
		{
			lowerPos = upperPos.down();
			IBlockState state = chunk.getBlockState(upperPos);
			IBlockState state1 = chunk.getBlockState(lowerPos);

			if (!state.getMaterial().blocksMovement() && state1.getMaterial().blocksMovement() && !state1.getBlock().isLeaves(state1, world, lowerPos) && !state1.getBlock().isFoliage(world, lowerPos))
			{
				break;
			}
		}

		return upperPos;
	}

	public static boolean canCreatureTypeSpawnAtLocation(EntityLiving.SpawnPlacementType spawnPlacementTypeIn, World worldIn, BlockPos pos)
	{
		if (!worldIn.getWorldBorder().contains(pos))
		{
			return false;
		}
		else
		{
			IBlockState iblockstate = worldIn.getBlockState(pos);

			if (spawnPlacementTypeIn == EntityLiving.SpawnPlacementType.IN_WATER)
			{
				return iblockstate.getMaterial() == Material.WATER && worldIn.getBlockState(pos.down()).getMaterial() == Material.WATER && !worldIn.getBlockState(pos.up()).isNormalCube();
			}
			else
			{
				BlockPos blockpos = pos.down();
				IBlockState state = worldIn.getBlockState(blockpos);

				if (!state.getBlock().canCreatureSpawn(state, worldIn, blockpos, spawnPlacementTypeIn))
				{
					return false;
				}
				else
				{
					Block block = worldIn.getBlockState(blockpos).getBlock();
					boolean flag = block != Blocks.BEDROCK && block != Blocks.BARRIER;
					return flag && WorldEntitySpawner.isValidEmptySpawnBlock(iblockstate) && WorldEntitySpawner.isValidEmptySpawnBlock(worldIn.getBlockState(pos.up()));
				}
			}
		}
	}
}
